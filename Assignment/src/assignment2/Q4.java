package assignment2;

import java.util.Scanner;

public class Q4 {

	public static void main(String[] args) {
		
		float money,cost,distance,distance2;
		String v;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Vehicle Type(Car/Bike): ");
		v=sc.nextLine();
		System.out.print("Enter the cost: ");
		money=sc.nextFloat();
		
		if(v.equalsIgnoreCase("Car")) {
			cost=money/115;
			distance=cost*8;
			distance2=distance*0.621371f;
			System.out.println("Using a " +v+" you can get "+String.format("%.2f", cost)+"L of petrol and can go up to "+String.format("%.2f", distance)+"KM"+" or "+String.format("%.2f", distance2)+" Miles");
			
		}else if(v.equalsIgnoreCase("Bike")) {
			cost=money/115;
			distance=cost*20;
			distance2=distance*0.621371f;
			System.out.println("Using a " +v+" you can get "+String.format("%.2f", cost)+"L of petrol and can go up to "+String.format("%.2f", distance)+"KM"+" or "+String.format("%.2f", distance2)+" Miles");
			
		}else{
			System.out.println("Invalid Vehicle Type!!!");
		}

	}

}
