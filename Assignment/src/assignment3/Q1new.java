package assignment3;

import java.util.Scanner;

public class Q1new {

	public static void main(String[] args) {
		
		int ticket,refre;
		char refreshment,coupon,circle;
		double cost,finalcost;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter the no of ticket:");
		ticket=sc.nextInt();
		sc.nextLine();
		
		System.out.print("Do you want refreshment:");
		refreshment=sc.next().charAt(0);
		
		System.out.print("Do you have coupon code");
		coupon=sc.next().charAt(0);
		
		System.out.print("Enter the circle:");
		circle=sc.next().charAt(0);
		
		cost=ticket*75;
		refre=ticket*50;//refreshment
		
		if(circle != 'k' && circle != 'q' && refreshment != 'y' && refreshment != 'n' && coupon != 'y' && coupon != 'n') {
			System.out.println("Invalid Input");
		}else {
		if(ticket<5 || ticket>40) {
			System.out.println("Invalid");
		}else {
			
			if(ticket<=20) {
				if(refreshment == 'y') {
					if(coupon == 'y') {
						if(circle == 'k') {
							finalcost=(cost*0.98f)+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}else {
							finalcost=(2*cost*0.98f)+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}
			
					}else {
						if(circle == 'k') {
							finalcost=cost+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}else {
							finalcost=(2*cost)+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}

					}
				}else {
					if(coupon == 'y') {
						if(circle == 'k') {
						finalcost=cost*0.98f;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}else {
						finalcost=2*cost*0.98f;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}
		
				}else {
					if(circle == 'k') {
						finalcost=cost;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}else {
						finalcost=2*cost;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}

				}
				}
			}else {//ticket more than 20

				if(refreshment == 'y') {
					if(coupon == 'y') {
						if(circle == 'k') {
							finalcost=(cost*0.98f*0.9f)+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}else {
							finalcost=(2*cost*0.98f*0.9f)+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}
			
					}else {
						if(circle == 'k') {
							finalcost=0.9f*cost+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}else {
							finalcost=(2*cost*0.9f)+refre;
							System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
						}

					}
				}else {
					if(coupon == 'y') {
						if(circle == 'k') {
						finalcost=cost*0.98f*0.9f;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}else {
						finalcost=2*cost*0.98f*0.9f;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}
		
				}else {
					if(circle == 'k') {
						finalcost=cost*0.9f;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}else {
						finalcost=2*cost*0.9f;
						System.out.println("Ticket cost:"+String.format("%.2f", finalcost));
					}

				}
				}
			}
		}
	}
	}

}
